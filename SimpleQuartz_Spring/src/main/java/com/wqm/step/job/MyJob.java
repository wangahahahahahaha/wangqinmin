package com.wqm.step.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import com.wqm.step.service.DemoService;

public class MyJob implements Job {

	@Autowired
	private DemoService demoService;

	public void execute(JobExecutionContext context) throws JobExecutionException {
		// 执行service接口的代码
		System.out.println(demoService.showMyName());
	}

}
