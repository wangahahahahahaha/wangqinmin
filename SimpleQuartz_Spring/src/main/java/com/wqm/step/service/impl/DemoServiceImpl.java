package com.wqm.step.service.impl;

import org.springframework.stereotype.Service;

import com.wqm.step.service.DemoService;

@Service
//@Transactional
public class DemoServiceImpl implements DemoService {

	public String showMyName() {
		return "My name is wangqinmin";
	}

}
